import React from 'react';
import { Meteor } from 'meteor/meteor';
import { Router, Route, browserHistory} from 'react-router';
import Login from '../ui/Login';
import Signup from '../ui/Signup';
import Link from '../ui/Link';
import NotFound from '../ui/NotFound';

const authenticatedPages = ['/links'];
const unauthenticatedPages = ['/', '/signup'];


onEnterPublicPages = () => {
  Meteor.userId() && browserHistory.replace('/links');
};

onEnterPrivatePages = () => {
  !Meteor.userId() && browserHistory.replace('/') 
};

export const onAuthChange = (isAuthenticated) => {
  const pathname = browserHistory.getCurrentLocation().pathname;
  const isAuthenticatedPage = authenticatedPages.includes(pathname);
  const isUnauthenticatedPage = unauthenticatedPages.includes(pathname);

  if(isUnauthenticatedPage && isAuthenticated) {
  browserHistory.replace('/links');
  } else if (isAuthenticatedPage && !isAuthenticated) {
  browserHistory.replace('/')
  }
};


export const routes = (
  <Router history={browserHistory}>
    <Route path="/" component={Login} onEnter={this.onEnterPublicPages}/>
    <Route path="/signup" component={Signup} onEnter={this.onEnterPublicPages}/>
    <Route path="/links" component={Link} onEnter={this.onEnterPrivatePages}/>
    <Route path="*" component={NotFound} />
  </Router>
); 