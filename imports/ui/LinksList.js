import React, { Component } from 'react';
import { Tracker } from 'meteor/tracker';
import { Meteor } from 'meteor/meteor';
import { Session } from 'meteor/session';
import FlipMove from 'react-flip-move';
import { Links } from '../api/links';
import LinksListItem from './LinksListItem';



export default class extends Component {
    constructor(props) {
        super(props);
        this.state = {
            links: [],
        }
    }

    componentDidMount() {
        console.log('Component did mount');
        this.linksTracker = Tracker.autorun(() => {
            Meteor.subscribe('linksPub');
            const links = Links.find({
                visible: Session.get('showVisible')
            }, { sort: { createdAt: -1 }}).fetch();
            this.setState({
                links
            })
        });
    };

    componentWillUnmount() {
        console.log('Component will unmount');
        this.linksTracker.stop();
    };


    renderLinksListItems = () => {
        if(this.state.links.length === 0) {
            return (
                <div className="item">
                    <p className="item__not-found">No links Found!</p>
                </div>
            );
        } else {
            return this.state.links.map((link) => {
                const shortUrl = Meteor.absoluteUrl(link._id);
                return <LinksListItem key={link._id} shortUrl={shortUrl} {...link} />
            });
        }
    };
    


    render() {
        return (
            <div>
                <FlipMove maintainContainerHeight={true}>
                    {this.renderLinksListItems()}
                </FlipMove>
            </div>
        );
    }
} 