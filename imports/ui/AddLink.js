import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import Modal from 'react-modal';



export default class AddLink extends Component {
    constructor(props) {
        super(props);
        this.state = {
            url: '',
            isModalOpen: false,
            error: ''
        }
    }

    onSubmit = (e) => {
        const { url } = this.state;
        e.preventDefault();
        
        Meteor.call('links.insert', url, (err, res) => {
            if(!err) {
                this.handleModalClose();
            } else {
                this.setState({
                    error: err.reason
                })
            }
        });
    }

    onChange = (e) => {
        this.setState({
            url: e.target.value
        })
    }

    handleModalClose = () => {
        this.setState({
            url: '',
            isModalOpen: false,
            error: '',
        })
    }

    render() {
        return (
            <div>
                <button className="button" onClick={() => this.setState({isModalOpen: true})}>+ Add link</button>
                <Modal
                isOpen={this.state.isModalOpen}
                contentLabel="Add link"
                onAfterOpen={() => this.refs.url.focus()}
                onRequestClose={this.handleModalClose}
                ariaHideApp={false}
                className="boxed-view__box"
                overlayClassName="boxed-view boxed-view--modal">
                    <h1>Add your links</h1>
                    {this.state.error ? <p>{this.state.error}</p> : undefined }
                    <form className="boxed-view__form" onSubmit={this.onSubmit}>
                        <input
                        type="text"
                        value={this.state.url}
                        placeholder="URL"
                        ref="url"
                        onChange={this.onChange}/>
                        <button className="button">+ Add link</button>
                        <button type="button" className="button button--secondary" onClick={this.handleModalClose}>Close</button>
                    </form>
                </Modal>
            </div>
        );
    }
}