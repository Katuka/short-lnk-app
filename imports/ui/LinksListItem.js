import React, { Component } from 'react';
import { Meteor } from 'meteor/meteor';
import moment from 'moment';
import propTypes from 'prop-types';
import Clipboard from 'clipboard';


export default class LinksListItem extends Component {

    constructor(props) {
        super(props);
        this.state = {
            justCopied: false,
        }
    }
    

    componentDidMount() {
       this.clipboard = new Clipboard(this.refs.copy);

       this.clipboard.on('success', () => {
            this.setState({
                justCopied: true,
            });
            setTimeout(() => {
            this.setState({
                justCopied: false,
            })
            }, 2000);
       }).on('error', () => {
           alert('Unable to copy to clipboard, please manually copy.')
       })
    }
    
    componentWillUnmount() {
        this.clipboard.destroy();
    }

    renderStats = () => {
        const visitMessage = this.props.visitedCount === 1 ? 'Visit' : 'Visits';
        this.visitedMessage = null;

        if (typeof this.props.lastVisitedAt === 'number') {
            this.visitedMessage = `(visited ${moment(this.props.lastVisitedAt).fromNow()})`;

        }
        return <p className="item__message">{this.props.visitedCount} {visitMessage} - {this.visitedMessage}</p> 
    }

    onLinkRemove = (_id) => {
        Meteor.call('link.remove', this.props._id)
    }

    render() {
        return (
            <div className="item">
                <h3>{this.props.url}</h3>
                <p className="item__message">{this.props.shortUrl}</p>
                {/* <p>{this.props.visible.toString()}</p> */}
                {this.renderStats()}
                
                <a className="button button--link button--pill" href={this.props.shortUrl} target="_blank">
                    Visit
                </a>
                <button className="button button--pill" ref="copy" data-clipboard-text={this.props.shortUrl}>{this.state.justCopied ? 'Copied' : 'Copy' }</button>
                <button className="button button--pill" onClick={() => {
                    Meteor.call('links.setVisibility', this.props._id, !this.props.visible)
                }}>{this.props.visible ? 'Hide' : 'Unhide' }</button>
                <button className="button button--pill" onClick={() => this.onLinkRemove(this.props._id)}>
                    {/* &times; */}
                    remove
                </button> 
            </div>
        );
    }
}

LinksListItem.propTypes = {
    url: propTypes.string.isRequired,
    shortUrl: propTypes.string.isRequired,
    _id: propTypes.string.isRequired,
    userId: propTypes.string.isRequired,
    visible: propTypes.bool.isRequired,
    visitedCount: propTypes.number.isRequired,
    lastVisitedAt: propTypes.number
}