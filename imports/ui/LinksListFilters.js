import React, { Component } from 'react';
import { Session } from 'meteor/session';
import { Tracker } from 'meteor/tracker';


export default class LinksListFilters extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showVisible: true,
        }
    }

    componentDidMount() {
        this.sessionTracker = Tracker.autorun(() => {
            const showVisible = Session.get('showVisible');
            this.setState({
                showVisible
            })
        })
    }

    componentWillUnmount() {
        this.sessionTracker.stop();
    }

    onClick = () => {
        this.setState({
            showVisible: !this.state.showVisible
        })
        Session.set('showVisible', !e.target.checked)
    }

    render() {
        return (
            <div>
                <lable className="checkbox" htmlFor="check">
                    <input
                    className="checkbox__box"
                    checked={!this.state.showVisible}
                    type="checkbox"
                    onChange={(e) => {
                        Session.set('showVisible', !e.target.checked)                    
                    }}
                    id="check"  
                    />
                    show hidden links 
                </lable>
            </div>
        );
    }
}